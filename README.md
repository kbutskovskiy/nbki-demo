# Тестовое задание для НБКИ Буцковского Кирилла
## Запуск
Для запуска необходимо убедиться, что все зависимости были установлены и запустить класс NbkiDemoApplication. В этот момент в памяти создастся H2, после чего с ней можно будет работать.
## Ручки (Endpoints)
- http://localhost:8080/nbki/client/get-by-id/{id}
- http://localhost:8080/nbki/client/add-client
- http://localhost:8080/nbki/client/update/{id}
- http://localhost:8080/nbki/client/delete/{id}
## Тесты
Тесты расположены по пути src/test/java/ru/nbki/nbkidemo