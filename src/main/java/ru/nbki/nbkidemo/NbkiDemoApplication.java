package ru.nbki.nbkidemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NbkiDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(NbkiDemoApplication.class, args);
    }

}
