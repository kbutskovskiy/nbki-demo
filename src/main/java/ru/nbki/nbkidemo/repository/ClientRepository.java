package ru.nbki.nbkidemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nbki.nbkidemo.entity.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

}
