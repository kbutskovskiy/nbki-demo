package ru.nbki.nbkidemo.dto;

import java.time.LocalDate;
import lombok.Data;

@Data
public class ClientDto {

    private Long id;
    private String fullName;
    private LocalDate birthDate;
    private String email;
    private String phoneNumber;
    private Integer creditScore;
}
