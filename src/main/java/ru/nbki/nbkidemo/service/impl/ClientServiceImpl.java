package ru.nbki.nbkidemo.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.nbki.nbkidemo.dto.ClientDto;
import ru.nbki.nbkidemo.entity.Client;
import ru.nbki.nbkidemo.exception.ResourceNotFoundException;
import ru.nbki.nbkidemo.repository.ClientRepository;
import ru.nbki.nbkidemo.service.ClientService;

@Service
@RequiredArgsConstructor
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;

    @Override
    public ClientDto updateClient(Long id, ClientDto clientDto) {
        Client client = clientRepository.findById(id).orElse(null);
        if (client == null) {
            throw new ResourceNotFoundException ("Client not found. Nothing to update");
        }
        client.setFullName(clientDto.getFullName());
        client.setBirthDate(clientDto.getBirthDate());
        client.setEmail(clientDto.getEmail());
        client.setPhoneNumber(clientDto.getPhoneNumber());
        client.setCreditScore(clientDto.getCreditScore());

        Client updatedClient = clientRepository.save(client);
        return mapToDto(updatedClient);
    }

    @Override
    public ClientDto addClient(ClientDto clientDto) {
        Client client = mapToEntity(clientDto);
        Client savedClient = clientRepository.save(client);
        return mapToDto(savedClient);
    }

    @Override
    public ClientDto getClientById(Long id) {
        Client client = clientRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Client not found"));
        return mapToDto(client);
    }

    private ClientDto mapToDto(Client client) {
        if (client == null) {
            return null;
        }
        ClientDto clientDto = new ClientDto();
        clientDto.setId(client.getId());
        clientDto.setFullName(client.getFullName());
        clientDto.setBirthDate(client.getBirthDate());
        clientDto.setEmail(client.getEmail());
        clientDto.setPhoneNumber(client.getPhoneNumber());
        clientDto.setCreditScore(client.getCreditScore());
        return clientDto;
    }

    private Client mapToEntity(ClientDto clientDto) {
        Client client = new Client();
        client.setFullName(clientDto.getFullName());
        client.setBirthDate(clientDto.getBirthDate());
        client.setEmail(clientDto.getEmail());
        client.setPhoneNumber(clientDto.getPhoneNumber());
        client.setCreditScore(clientDto.getCreditScore());
        return client;
    }

    @Override
    public boolean deleteClientById(Long id) {
        if (clientRepository.existsById(id)) {
            clientRepository.deleteById(id);
            return true;
        }
        return false;
    }
}
