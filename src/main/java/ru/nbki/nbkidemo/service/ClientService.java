package ru.nbki.nbkidemo.service;

import ru.nbki.nbkidemo.dto.ClientDto;

public interface ClientService {

    ClientDto updateClient(Long id, ClientDto clientDto);
    ClientDto addClient(ClientDto clientDto);
    ClientDto getClientById(Long clientId);
    boolean deleteClientById(Long id);
}
