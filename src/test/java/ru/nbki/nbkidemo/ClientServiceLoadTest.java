package ru.nbki.nbkidemo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadLocalRandom;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.nbki.nbkidemo.dto.ClientDto;
import ru.nbki.nbkidemo.service.ClientService;

@SpringBootTest
public class ClientServiceLoadTest {

    @Autowired
    private ClientService clientService;

    private static final int NUM_THREADS = 100; // 100 connections
    private static final int REQUESTS_PER_THREAD = 10000;

    public List<Long> populateDatabaseWithClients() {
        List<Long> clientIds = new ArrayList<>();
        for (int i = 0; i < 1_000_000; i++) {
            ClientDto clientDto = new ClientDto();
            clientDto.setFullName("Client " + i);
            clientDto.setBirthDate(LocalDate.of(1980 + ThreadLocalRandom.current().nextInt(40),
                    ThreadLocalRandom.current().nextInt(1, 13),
                    ThreadLocalRandom.current().nextInt(1, 29)));
            clientDto.setEmail("client" + i + "@example.com");
            clientDto.setPhoneNumber("1234567890");
            clientDto.setCreditScore(ThreadLocalRandom.current().nextInt(300, 851));

            ClientDto createdClient = clientService.addClient(clientDto);
            clientIds.add(createdClient.getId());
        }
        return clientIds;
    }

    @Test
    public void testGetClientByIdPerformance() throws InterruptedException {
        List<Long> clientIds = populateDatabaseWithClients(); // populate db

        ExecutorService executorService = Executors.newFixedThreadPool(NUM_THREADS);
        CountDownLatch latch = new CountDownLatch(NUM_THREADS);
        List<Long> responseTimes = Collections.synchronizedList(new ArrayList<>());

        long startTestTime = System.nanoTime();

        for (int i = 0; i < NUM_THREADS; i++) {
            executorService.execute(() -> {
                List<Long> threadClientIds = new ArrayList<>(clientIds);
                Collections.shuffle(threadClientIds); // random access for any thread
                for (Long clientId : threadClientIds.subList(0, REQUESTS_PER_THREAD)) {
                    long startTime = System.nanoTime();
                    clientService.getClientById(clientId);
                    long responseTime = System.nanoTime() - startTime;
                    responseTimes.add(responseTime);
                }
                latch.countDown();
            });
        }

        latch.await();
        executorService.shutdown();

        long endTestTime = System.nanoTime();
        long totalTestTime = endTestTime - startTestTime;

        printStatistics(responseTimes, totalTestTime);
    }

    private void printStatistics(List<Long> responseTimes, Long totalTestTime) {
        Collections.sort(responseTimes);
        long totalRequests = responseTimes.size();

        long median = responseTimes.get(responseTimes.size() / 2);
        long percentile95 = responseTimes.get((int) (responseTimes.size() * 0.95));
        long percentile99 = responseTimes.get((int) (responseTimes.size() * 0.99));

        System.out.println("Total requests: " + totalRequests);
        System.out.println("Total time: " + totalTestTime / 1_000_000 + " ms");
        System.out.println("Median response time: " + median / 1_000_000 + " ms");
        System.out.println("95th percentile response time: " + percentile95 / 1_000_000 + " ms");
        System.out.println("99th percentile response time: " + percentile99 / 1_000_000 + " ms");
    }

}
