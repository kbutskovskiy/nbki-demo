package ru.nbki.nbkidemo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import ru.nbki.nbkidemo.dto.ClientDto;

import java.time.LocalDate;
import ru.nbki.nbkidemo.service.ClientService;

@SpringBootTest
public class ClientServiceTest {

    @Autowired
    private ClientService clientService;

    @Test
    @Transactional
    public void testCreateManyClients() {
        for (int i = 0; i < 100000; i++) {
            ClientDto client = new ClientDto();
            client.setFullName("Client " + i);
            client.setBirthDate(LocalDate.of(1990, 1, 1));
            client.setEmail("client" + i + "@example.com");
            client.setPhoneNumber("1234567890");
            client.setCreditScore(700);
            clientService.addClient(client);
        }
    }
}
